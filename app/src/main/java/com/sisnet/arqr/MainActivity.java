package com.sisnet.arqr;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.Timer;

public class MainActivity extends AppCompatActivity {
    ImageView i1,i2,i3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        i1=findViewById(R.id.image1);
        i2=findViewById(R.id.image2);
        i3=findViewById(R.id.image3);



        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                i1.animate().alpha(1).setDuration(600);
            }
        },400);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                i2.animate().alpha(1).setDuration(600);
            }
        },800);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                i3.animate().alpha(1).setDuration(600);
            }
        },1200);


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startQrRender();
            }
        },2000);


    }

    private void startQrRender() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED){

                requestPermissions(new String[]{Manifest.permission.CAMERA},1);
            }else {
                startActivity(new Intent(MainActivity.this, QrCodeTrackerActivity.class));
                finish();

            }
        }else {
            startActivity(new Intent(MainActivity.this, QrCodeTrackerActivity.class));
            finish();


        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1){
//            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_DENIED){
//                Log.i("startQrRender", "startQrRender: 8");
//                Toast.makeText(this, getResources().getString(R.string.toast_permissin), Toast.LENGTH_SHORT).show();
//            }
            startQrRender();
        }
    }
}
