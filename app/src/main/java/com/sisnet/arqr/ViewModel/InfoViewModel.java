package com.sisnet.arqr.ViewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.sisnet.arqr.model.Info;
import com.sisnet.arqr.model.InfoReposotory;
import com.sisnet.arqr.net.Resource;

public class InfoViewModel extends AndroidViewModel {
    InfoReposotory reposotory;

    public InfoViewModel(@NonNull Application application) {
        super(application);
        reposotory = new InfoReposotory(application);
    }

    public LiveData<Resource<Info>> getInfo(int id){
        return reposotory.getInfoQrCode(id);
    }
}
