package com.sisnet.arqr.net

import android.content.Context
import com.google.gson.JsonParseException
import com.sisnet.arqr.R
import org.json.JSONException
import retrofit2.Response
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import java.util.concurrent.TimeoutException

enum class ApiError {
    ERROR_204, ERROR_400, ERROR_401, ERROR_404, ERROR_405, ERROR_429,
    ERROR_500, ERROR_TIME_OUT, ERROR_DISCONNECTED, ERROR_PARSE, ERROR_UNKNOWN,
    ERROR_NO_CONTENT, ERROR_CODE_0
}


fun ApiError.getStringMessage(context: Context?): String? {
    context?.let {
        return when (this) {
            ApiError.ERROR_204, ApiError.ERROR_NO_CONTENT -> context.resources.getString(R.string.ERROR_204)
            ApiError.ERROR_404 -> context.resources.getString(R.string.ERROR_404)
            ApiError.ERROR_429 -> context.resources.getString(R.string.ERROR_429)
            ApiError.ERROR_500 -> context.resources.getString(R.string.ERROR_500)
            ApiError.ERROR_DISCONNECTED -> context.resources.getString(R.string.ERROR_DISCONNECTED)
            ApiError.ERROR_UNKNOWN -> context.resources.getString(R.string.ERROR_UNKNOWN)
            ApiError.ERROR_TIME_OUT -> context.resources.getString(R.string.ERROR_TIME_OUT)
            else -> context.resources.getString(R.string.somethingWentWrong)
        }
    }
    return null
}


fun Throwable.getApiError(): ApiError {
    return when (this) {
        is UnknownHostException, is ConnectException -> ApiError.ERROR_DISCONNECTED
        is JsonParseException, is JSONException -> ApiError.ERROR_PARSE
        is TimeoutException, is SocketTimeoutException -> ApiError.ERROR_TIME_OUT
        else -> ApiError.ERROR_UNKNOWN
    }
}


fun <T> Response<T>.getApiError(): ApiError {
    return when (this.code()) {
        204 -> ApiError.ERROR_204
        400 -> ApiError.ERROR_400
        401 -> ApiError.ERROR_401
        404 -> ApiError.ERROR_404
        405 -> ApiError.ERROR_405
        429 -> ApiError.ERROR_429
        500 -> ApiError.ERROR_500
        else -> ApiError.ERROR_UNKNOWN
    }
}