package com.sisnet.arqr.net;

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}
