package com.sisnet.arqr.net

data class Resource<out T> @JvmOverloads constructor(val status: Status, val data: T?, val message: String?, val error: ApiError? = null) {
    companion object {
        fun <T> success(data: T?): Resource<T> {
            return Resource(Status.SUCCESS, data, null)
        }

        fun <T> error(msg: String?, data: T?, error: ApiError): Resource<T> {
            return Resource(Status.ERROR, data, msg, error)
        }

        fun <T> loading(data: T?): Resource<T> {
            return Resource(Status.LOADING, data, null)
        }
    }
}
