package com.sisnet.arqr.net;

import android.content.Context;
import android.graphics.Color;

import com.sisnet.arqr.BuildConfig;

import java.io.File;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitBuilder {
    public static final String BaseUrl = "http://sisapp.ir/";
    private static Api api;
    private static Retrofit retrofit;
    private static OkHttpClient.Builder httpClient;

    public static Api RetrofitConfig(){
        if (api==null){
            api = provideRetrofit(provideOkHttpClient()).create(Api.class);
        }
        return api;
    }

    static Retrofit provideRetrofit(OkHttpClient.Builder httpClient) {
        if (retrofit==null){
            retrofit = new Retrofit.Builder()
                    .baseUrl(BaseUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(new LiveDataCallAdapterFactory())
                    .client(httpClient.build())
                    .build();
        }
        return retrofit;
    }

    static OkHttpClient.Builder provideOkHttpClient() {
        if (httpClient==null) {
            httpClient = new OkHttpClient.Builder();
            httpClient.readTimeout(30, TimeUnit.SECONDS)
                    .connectTimeout(30, TimeUnit.SECONDS)
                    .writeTimeout(30, TimeUnit.SECONDS)
                    .build();

            if (BuildConfig.DEBUG)
                httpClient.addInterceptor(new HttpLoggingInterceptor()
                        .setLevel(HttpLoggingInterceptor.Level.BODY));

            httpClient.addInterceptor(chain -> {
                Request original = chain.request();
                Request request = original.newBuilder()
                        .header("version", String.valueOf(BuildConfig.VERSION_CODE))
                        .method(original.method(), original.body())
                        .build();

                return chain.proceed(request);
            });
        }
        return httpClient;

    }

}

