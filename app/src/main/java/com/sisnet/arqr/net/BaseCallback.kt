package com.sisnet.arqr.net

import com.google.gson.annotations.SerializedName

open class BaseCallback<T>(
        @SerializedName("data")
        val data: T,
        @SerializedName("message")
        val message: String,
        @SerializedName("status")
        val status: Int
)