package com.sisnet.arqr.net

import androidx.annotation.MainThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData

abstract class NetworkResource<RequestType> {

    private val result = MediatorLiveData<Resource<RequestType>>()


    init {

        result.value = Resource.loading(null)
        fetchFromNetwork()
    }

    @MainThread
    private fun setValue(newValue: Resource<RequestType>) {
        if (result.value != newValue) {
            result.value = newValue
        }
    }

    private fun fetchFromNetwork() {
        val apiResponse = createCall()

        result.addSource(apiResponse) { response ->
            result.removeSource(apiResponse)
            when (response) {
                is ApiSuccessResponse -> {
                    if (response.body.status == 1)
                        setValue(Resource.success(response.body.data))
                    else
                        setValue(Resource.error(response.body.message, response.body.data, ApiError.ERROR_CODE_0))
                }
                is ApiErrorResponse -> {

                    onFetchFailed()
                    setValue(Resource.error(response.errorMessage, null, response.error))

                }
            }
        }
    }

    protected open fun onFetchFailed() {}

    fun asLiveData() = result as LiveData<Resource<RequestType>>

    @MainThread
    protected abstract fun createCall(): LiveData<ApiResponse<BaseCallback<RequestType>>>
}