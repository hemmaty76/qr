package com.sisnet.arqr.net

import android.util.Log
import com.google.gson.Gson
import retrofit2.Response


@Suppress("unused")
sealed class ApiResponse<T> {
    companion object {
        val gson = Gson()

        fun <T> create(error: Throwable): ApiErrorResponse<T> {
            return ApiErrorResponse(null, error.getApiError())
        }

        fun <T> create(response: Response<T>): ApiResponse<T> {
            return if (response.isSuccessful) {
                val body = response.body()
                if (body != null && response.code() == 200)
                    ApiSuccessResponse(body)
                else
                    ApiErrorResponse(null, response.getApiError())
            } else {
                var msg: String? = null
                try {
                    val errorBody = gson.fromJson(response.errorBody()?.string(),
                            BaseCallback::class.java)
                    msg = errorBody?.message
                } catch (e: Exception) {
                    e.printStackTrace()
                }

                ApiErrorResponse(msg, response.getApiError())
            }
        }
    }
}

/**
 * separate class for HTTP 204 responses so that we can make ApiSuccessResponse's body non-null.
 */
//class ApiEmptyResponse<T> : ApiResponse<T>()

data class ApiSuccessResponse<T>(val body: T) : ApiResponse<T>()

/**
 * we user error message in case of when we want to show error message that come from server side
 * such as when use enter incorrect password for login or enter incorrect input field
 * otherwise we use just ApiError to handle error
 */
data class ApiErrorResponse<T>(val errorMessage: String?, val error: ApiError) : ApiResponse<T>()

