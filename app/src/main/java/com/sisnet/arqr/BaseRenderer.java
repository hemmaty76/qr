package com.sisnet.arqr;

import android.opengl.Matrix;

import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

public abstract class BaseRenderer {
     int type = -2;

    public float[] localMvpMatrix = new float[16];
    public float [] projectionMatrix = new float[16];
    public float[] modelMatrix = new float[16];
    public float[] translation = new float[16];
    public float[] scale = new float[16];
    public float[] rotation = new float[16];
    public float[] transform = new float[16];

    public int shaderProgramId = 0;
    public int positionHandle;
    public int colorHandle;
    public int textureCoordHandle;
    public int mvpMatrixHandle;
    public int textureHandle;

    public int[] textureNames;
    public int[] textureHandles;

    public FloatBuffer vertexBuffer;
    public ShortBuffer indexBuffer;
    public FloatBuffer colorBuffer;
    public FloatBuffer textureCoordBuff;

    public BaseRenderer() {
        Matrix.setIdentityM(localMvpMatrix, 0);
        Matrix.setIdentityM(projectionMatrix, 0);
        Matrix.setIdentityM(modelMatrix, 0);
        Matrix.setIdentityM(translation, 0);
        Matrix.setIdentityM(scale, 0);
        Matrix.setIdentityM(rotation, 0);
        Matrix.setIdentityM(transform, 0);
    }

    public void setProjectionMatrix(float [] projectionMatrix) {
        this.projectionMatrix = projectionMatrix;
    }

    public void setScale(float x, float y, float z) {
        Matrix.setIdentityM(scale, 0);
        Matrix.scaleM(scale, 0, x, y, z);
    }

    public void setTranslate(float x, float y, float z) {
        Matrix.setIdentityM(translation, 0);
        Matrix.translateM(translation, 0, x, y, z);
    }

    public void setRotation(float angle, float x, float y, float z) {
        Matrix.setIdentityM(rotation, 0);
        Matrix.rotateM(rotation, 0, angle, x, y, z);
    }

    public void setTransform(float[] transform) {
        System.arraycopy(transform, 0, this.transform, 0, transform.length);
    }

    public void draw() {

    }


}