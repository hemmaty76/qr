package com.sisnet.arqr.model;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.sisnet.arqr.net.Api;
import com.sisnet.arqr.net.ApiResponse;
import com.sisnet.arqr.net.BaseCallback;
import com.sisnet.arqr.net.NetworkResource;
import com.sisnet.arqr.net.Resource;
import com.sisnet.arqr.net.RetrofitBuilder;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class InfoReposotory {
    private Application application;
    private Api api;

    private MutableLiveData<Info> mutableLiveData = new MutableLiveData<>();
    public InfoReposotory(Application application) {
        this.application = application;
        api = RetrofitBuilder.RetrofitConfig();
    }


    public LiveData<Resource<Info>> getInfoQrCode(int id){
        return new NetworkResource<Info>(){
            @NotNull
            @Override
            protected LiveData<ApiResponse<BaseCallback<Info>>> createCall() {
                return api.getInfo(id);
            }
        }.asLiveData();
    }

}
