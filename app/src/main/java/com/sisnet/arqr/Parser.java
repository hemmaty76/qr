package com.sisnet.arqr;

import android.util.Log;

import it.auron.library.geolocation.GeoCard;
import it.auron.library.geolocation.GeoCardParser;
import it.auron.library.mecard.MeCard;
import it.auron.library.mecard.MeCardParser;
import it.auron.library.vcard.VCard;
import it.auron.library.vcard.VCardParser;
import it.auron.library.vevent.VEvent;
import it.auron.library.vevent.VEventParser;
import it.auron.library.wifi.WifiCard;
import it.auron.library.wifi.WifiCardParser;

public class Parser  {
    public static final int MAIL_TYPE = 0;
    public static final int EVENT_TYPE = 1;
    public static final int LOCATION_TYPE = 2;
    public static final int MECARD_TYPE = 3;
    public static final int TELL_TYPE = 4;
    public static final int SMS_TYPE = 5;
    public static final int VCARD_TYPE = 6;
    public static final int WIFI_TYPE = 7;
    public static final int URL_TYPE = 9;
    public static final int TEXT_TYPE = 8;
    public static final String TAG = "erroreparser";
    int type;

    /*mail*/ private String addresMail;private String subjectMail;private String bodyMail;
    /*sms*/ private String phoneSms;private String bodySms;
    /*tell*/ private String phoneTell;
    /*location*/ private GeoCard geoCard;
    /*text*/ private String text;
    /*event*/private  VEvent vEvent;
    /*Card*/ private MeCard meCard;
    /*vcard*/ private VCard vCard;
    /*wifi*/ private WifiCard wifiCard;
    /*url*/ private String url;

    public String getUrl() {
        return url;
    }

    public Parser(String url){
        String result = url.trim();

        if (result.startsWith("http")) {
            type = URL_TYPE;
            this.url = result;
        }else if (result.startsWith("mailto:")){
            type = MAIL_TYPE;
            try {
                result = result.substring(7,result.length());
                addresMail = result.substring(0,result.indexOf("?subject="));
                result = result.substring(addresMail.length()+9,result.length());
                String[] splited = result.split("&body=");
                subjectMail = splited[0];
                for (int i = 1;i<splited.length;i++)bodyMail += splited[i];
            }catch (Exception e){
                Log.i(TAG, "Parser: "+ e.getMessage());
            }
        }else if (result.startsWith("SMSTO:")){
            type = SMS_TYPE;
            result = result.substring(6,result.length());
            String[] splited = result.split(":");
            phoneSms = splited[0];
            for (int i = 1;i<splited.length;i++)
                bodySms+=splited[i];
        }else if (result.startsWith("tel:")){
            type = TELL_TYPE;
            phoneTell = result.substring(4,result.length());
        } else if (VEventParser.isVEvent(result)){
            type = EVENT_TYPE;
            vEvent = VEventParser.parse(result);
        }else if (MeCardParser.isMeCard(result)){
            type = MECARD_TYPE;
            meCard = MeCardParser.parse(result);
        }else if (VCardParser.isVCard(result)){
            type = VCARD_TYPE;
            vCard = VCardParser.parse(result);
        }else if (WifiCardParser.isWifi(result)){
            type = WIFI_TYPE;
            wifiCard = WifiCardParser.parse(result);
        }else if (GeoCardParser.isGeoCard(result)){
            type = LOCATION_TYPE;
            geoCard = GeoCardParser.parse(result);
        }else {
            type = TEXT_TYPE;
            text = result;
        }
    }


    public int getType() {
        return type;
    }

    public String getAddresMail() {
        return addresMail;
    }

    public String getSubjectMail() {
        return subjectMail;
    }

    public String getBodyMail() {
        return bodyMail;
    }

    public VEvent getvEvent() {
        return vEvent;
    }


    public GeoCard getGeoCard() {
        return geoCard;
    }

    public MeCard getMeCard() {
        return meCard;
    }

    public String getPhoneTell() {
        return phoneTell;
    }

    public String getPhoneSms() {
        return phoneSms;
    }

    public String getBodySms() {
        return bodySms;
    }

    public String getText() {
        return text;
    }

    public VCard getvCard() {
        return vCard;
    }

    public WifiCard getWifiCard() {
        return wifiCard;
    }
}
