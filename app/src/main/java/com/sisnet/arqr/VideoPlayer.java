package com.sisnet.arqr;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.graphics.SurfaceTexture;
import android.graphics.SurfaceTexture.OnFrameAvailableListener;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnBufferingUpdateListener;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.NetworkInfo;
import android.net.Uri;
import android.util.Log;
import android.view.Surface;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.video.VideoListener;
import java.util.concurrent.locks.ReentrantLock;

public class VideoPlayer{
    static final String TAG = VideoPlayer.class.getSimpleName();
    public static final int STATE_NONE = 0;
    public static final int STATE_READY = 1;
    public static final int STATE_PLAYING = 2;
    public static final int STATE_PAUSE = 3;
    public static final int STATE_DESTROYED = 4;
    public static final int STATE_ERROR = 100;
    public Context context = null;
//    public MediaPlayer mediaPlayer = null;
    public String path = "";
    public int videoWidth = 0;
    public int videoHeight = 0;
    public int videoTextureId = 0;
    public int fboId = 0;
    public SurfaceTexture surfaceTexture = null;
    public ReentrantLock surfaceTextureLock = new ReentrantLock();
    public boolean isTexUpdated = false;
    public boolean isTextureDrawable = false;
    public int state = 0;
    public VideoPlayer.VideoCompletionListener videoCompletionListener;
    public SimpleExoPlayer player ;
    public DefaultTrackSelector selector;
    public TrackSelection.Factory videoTrackSelectionFactory;

    public OnFrameAvailableListener onFrameAvailableListener = new OnFrameAvailableListener() {
        public void onFrameAvailable(SurfaceTexture surfaceTexture) {
            VideoPlayer.this.surfaceTextureLock.lock();
            VideoPlayer.this.isTexUpdated = true;
            VideoPlayer.this.surfaceTextureLock.unlock();
        }
    };



    public VideoPlayer(Context context) {
        Log.i(TAG, "Construct Video Player");
        this.context = context;
    }

    public boolean openVideo(String path) {
        if (this.player != null) {
            Log.w(TAG, "Video Player has a MediaPlayer already :" + path);
            return false;
        } else {
            videoTrackSelectionFactory = new AdaptiveTrackSelection.Factory(new DefaultBandwidthMeter());
            selector = new DefaultTrackSelector(videoTrackSelectionFactory);
            selector.buildUponParameters().setRendererDisabled(0,true);
            player = ExoPlayerFactory.newSimpleInstance(this.context,selector);
//            MediaSource source = new MediaSource(Uri.parse(path),new DefaultDataSourceFactory(this.context, "https://google.com"),new DefaultExtractorsFactory(),null,null);

            player.setPlayWhenReady(true);
 //           player.prepare(source);

            player.addVideoListener(new VideoListener() {
                @Override
                public void onVideoSizeChanged(int width, int height, int unappliedRotationDegrees, float pixelWidthHeightRatio) {
                    VideoPlayer.this.videoWidth = width;
                    VideoPlayer.this.videoHeight = height;
                    VideoPlayer.this.setState(1);
                    Log.e(VideoPlayer.TAG, "Video is prepared:" + VideoPlayer.this.path);
                }

                @Override
                public void onRenderedFirstFrame() {

                }
            });

            return true;
        }
    }

    public void setTexture(int destTexId) {
        this.surfaceTextureLock.lock();
        if (this.player != null) {
            this.videoTextureId = this.initVideoTexture();
            this.fboId = this.initFBO(destTexId);
            this.surfaceTexture = new SurfaceTexture(this.videoTextureId);
            this.player.setVideoSurface(new Surface(this.surfaceTexture));
            this.surfaceTexture.setOnFrameAvailableListener(this.onFrameAvailableListener);
        }

        this.surfaceTextureLock.unlock();
    }

    public void start() {
        this.surfaceTextureLock.lock();
        if (this.player != null) {
            if (!(this.player.getPlaybackState() == ExoPlayer.STATE_READY)) {
                this.player.setPlayWhenReady(true);
                this.setState(2);
            } else {
                Log.i(TAG, "Video is already playing:" + this.path);
            }
        } else {
            Log.e(TAG, "You should call openVideo() first");
        }

        this.surfaceTextureLock.unlock();
    }

    public long update() {
        long timeStamp = 0L;
        this.surfaceTextureLock.lock();
        if (this.isTexUpdated) {
            this.surfaceTexture.updateTexImage();
            this.copyTexture(this.fboId, this.videoTextureId, this.videoWidth, this.videoHeight);
            this.isTexUpdated = false;
            this.isTextureDrawable = true;
            timeStamp = this.surfaceTexture.getTimestamp();
        }

        this.surfaceTextureLock.unlock();
        return timeStamp;
    }

    public void setPosition(Long position) {
        if (this.player != null && ((this.player.getPlaybackState() == ExoPlayer.STATE_READY)&&(player.getPlayWhenReady()))) {
            this.player.seekTo(position);
        }

    }

    public Long getCurrentPosition() {
        return this.player != null ? this.player.getCurrentPosition() : 0;
    }

    public Long getDuration() {
        return this.player != null ? this.player.getDuration() : 0;
    }

    public void setOnCompletionListener(VideoPlayer.VideoCompletionListener listener) {
        this.videoCompletionListener = listener;
    }

    public void pause() {
        this.surfaceTextureLock.lock();
        if (this.player != null) {
            if (((this.player.getPlaybackState() == ExoPlayer.STATE_READY)&&(player.getPlayWhenReady()))) {
                this.player.setPlayWhenReady(false);
                this.setState(3);
                Log.i(TAG, "Pause video");
            }
        } else {
            Log.e(TAG, "You have not opened a video");
        }

        this.surfaceTextureLock.unlock();
    }
    

    public void destroy() {
        this.surfaceTextureLock.lock();
        if (this.player != null) {
            this.player.stop();
            this.player.release();
            this.player = null;
            this.setState(4);
        } else {
            Log.e(TAG, "You have not opened a video");
        }

        this.surfaceTextureLock.unlock();
    }

    public int getVideoWidth() {
        return this.videoWidth;
    }

    public int getVideoHeight() {
        return this.videoHeight;
    }

    public boolean isTextureDrawable() {
        return this.isTextureDrawable;
    }

    public synchronized void setState(int state) {
        this.state = state;
    }

    public synchronized int getState() {
        return this.state;
    }

    public native int initFBO(int var1);

    public native int initVideoTexture();

    public native void copyTexture(int var1, int var2, int var3, int var4);

    static {
        System.loadLibrary("VideoPlayer");
    }

    public interface VideoCompletionListener {
        void onComplete();
    }
}

