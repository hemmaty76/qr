package com.sisnet.arqr.Obj;

import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

/**
 * Contains all of the data for a .obj File
 * @author Iyaz
 *
 */
public class Model {

    private ShortBuffer indicesPositions;
    private FloatBuffer verticesPositions;
    private FloatBuffer verticesNormals;
    private FloatBuffer verticesColors;
    private String filename;

    private float[] vertices;
    private short[] indicesVertices;
    private float[] normals;
    private float[] colors;

    public Model(String filename, ShortBuffer indicesPositions, short[] indicesVertices, FloatBuffer verticesPositions, float[] vertices, FloatBuffer verticesNormals, float[] normals, FloatBuffer verticesColors, float[] colors) {
        this.filename = filename;
        this.verticesPositions = verticesPositions;
        this.verticesNormals = verticesNormals;
        this.indicesPositions = indicesPositions;
        this.verticesColors = verticesColors;
        this.indicesVertices = indicesVertices;
        this.vertices = vertices;
        this.normals = normals;
        this.colors = colors;
    }

    public float[] getVertices() {
        return vertices;
    }

    public short[] getIndicesVertices() {
        return indicesVertices;
    }

    public float[] getNormals() {
        return normals;
    }

    public float[] getColors() {
        return colors;
    }

    public Model(String filename, ShortBuffer indicesPositions, short[] indicesVertices, FloatBuffer verticesPositions, float[] vertices, FloatBuffer verticesNormals, float[] normals) {
        this.filename = filename;
        this.verticesPositions = verticesPositions;
        this.verticesNormals = verticesNormals;
        this.indicesPositions = indicesPositions;
        this.indicesVertices = indicesVertices;
        this.vertices = vertices;
        this.normals = normals;

    }

    public FloatBuffer getVerticesPositions() {
        return this.verticesPositions;
    }

    public ShortBuffer getIndicesPositions()
    {
        return this.indicesPositions;
    }

    public FloatBuffer getVerticesNormals()
    {
        return this.verticesNormals;
    }

    public FloatBuffer getVerticesColors()
    {
        return this.verticesColors;
    }


    public String getFilename()
    {
        return this.filename;
    }

}

