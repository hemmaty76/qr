package com.sisnet.arqr.Obj;


import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;
import android.content.Context;
import android.graphics.Bitmap;
import android.opengl.GLSurfaceView.Renderer;
import android.opengl.GLES20;
import android.opengl.GLUtils;
import com.sisnet.arqr.objloader.ObjLoader;
import android.opengl.Matrix;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;

import com.sisnet.arqr.BaseRenderer;
import com.sisnet.arqr.R;
import com.sisnet.arqr.ShaderUtil;

/**
 * Renderer for the Game GLSurfaceView
 * Some code taken from learnopengles.com
 * @author Iyaz
 *
 */
public class ObjRenderer extends BaseRenderer {


    private final ObjLoader loader;
    private AppCompatActivity context;
    public boolean isReadyObj = false;



//    private static final float[] VERTEX_BUFz = {
////            -0.5f, -0.5f, -0.5f,
////            0.5f, -0.5f, -0.5f,
////            0.5f, 0.5f, -0.5f,
////            -0.5f, 0.5f, -0.5f,
////
////            // 2. Bottom face
////            -0.5f, -0.5f, 0.5f,
////            -0.5f, 0.5f, 0.5f,
////            0.5f, 0.5f, 0.5f,
////            0.5f, -0.5f, 0.5f,
////
////            // 3. Back face
////            -0.5f, -0.5f, -0.5f,
////            -0.5f, -0.5f, 0.5f,
////            0.5f, -0.5f, 0.5f,
////            0.5f, -0.5f, -0.5f,
////
////            // 4. Right face
////            0.5f, -0.5f, -0.5f,
////            0.5f, -0.5f, 0.5f,
////            0.5f,  0.5f, 0.5f,
////            0.5f, 0.5f, -0.5f,
////
////            // 5. Front face
////            0.5f, 0.5f, -0.5f,
////            0.5f, 0.5f, 0.5f,
////            -0.5f, 0.5f, 0.5f,
////            -0.5f, 0.5f, -0.5f,
////
////            // 6. Left face
////            -0.5f, 0.5f, -0.5f,
////            -0.5f, 0.5f, 0.5f,
////            -0.5f, -0.5f, 0.5f,
////            -0.5f, -0.5f, -0.5f,
//
//            -0.5f, 0.5f, 0.0f,
//            -0.5f, -0.5f, 0.0f,
//            0.5f, -0.5f, 0.0f,
//            0.5f, 0.5f, 0.0f
//    };
//
//    private static short[] INDEX_BUF = {
////            1,  0, 3, 3, 2, 1,
////            4,  7, 6, 6, 5, 4,
////            8, 11,10,10, 9, 8,
////            13,12,15,15,14,13,
////            17,16,19,19,18,17,
////            21,20,23,23,22,21,
//            1, 0, 3, 3, 2, 1
//    };
//
//    private static final float[] TEXTURE_COORD_BUFz = {
////            0.167f, 0.100f,
////            0.833f, 0.100f,
////            0.833f, 0.500f,
////            0.167f, 0.500f,
////
////            0.167f, 0.667f,
////            0.833f, 0.667f,
////            0.833f, 1.000f,
////            0.167f, 1.000f,
////
////            0.167f, 0.000f,
////            0.833f, 0.000f,
////            0.833f, 0.100f,
////            0.167f, 0.100f,
////
////            0.833f, 0.100f,
////            1.000f, 0.100f,
////            1.000f, 0.500f,
////            0.833f, 0.500f,
////
////            0.167f, 0.000f,
////            0.833f, 0.000f,
////            0.833f, 0.100f,
////            0.167f, 0.100f,
////
////            0.833f, 0.500f,
////            0.833f, 0.100f,
////            1.000f, 0.100f,
////            1.000f, 0.500f,
//
//            0.0f, 1.0f,
//            0.0f, 0.0f,
//            1.0f, 0.0f,
//            1.0f, 1.0f,
//    };


    private static final String VERTEX_SHADER_SRC =
            "attribute vec4 a_position;\n" +
                    "attribute vec2 a_texCoord;\n" +
                    "varying vec2 v_texCoord;\n" +
                    "uniform mat4 u_mvpMatrix;\n" +
                    "void main()							\n" +
                    "{										\n" +
                    "	gl_Position = u_mvpMatrix * a_position;\n" +
                    "	v_texCoord = a_texCoord; 			\n" +
                    "}										\n";

    private static final String FRAGMENT_SHADER_SRC =
            "precision mediump float;\n" +
                    "varying vec2 v_texCoord;\n" +
                    "uniform sampler2D u_texture;\n" +

                    "void main(void)\n" +
                    "{\n" +
                    "	gl_FragColor = texture2D(u_texture, v_texCoord);\n" +
                    "}\n";


    public ObjRenderer(ObjLoader objLoader, AppCompatActivity context) {
        this.context = context;
        this.loader = objLoader;




        shaderProgramId = ShaderUtil.createProgram(VERTEX_SHADER_SRC, FRAGMENT_SHADER_SRC);

        positionHandle = GLES20.glGetAttribLocation(shaderProgramId, "a_position");
        textureCoordHandle = GLES20.glGetAttribLocation(shaderProgramId, "a_texCoord");
        mvpMatrixHandle = GLES20.glGetUniformLocation(shaderProgramId, "u_mvpMatrix");
        textureHandle = GLES20.glGetUniformLocation(shaderProgramId, "u_texture");

        textureNames = new int[1];

        GLES20.glGenTextures(1, textureNames, 0);
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textureNames[0]);
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE);
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE);



    }

    public void loadData(String file){
        if (textureCoordBuff == null){
            loader.load(file).observe(context, new Observer<Boolean>() {
                @Override
                public void onChanged(Boolean aBoolean) {
                    if (aBoolean){
                        FloatBuffer positions = ByteBuffer.allocateDirect(loader.positions.length * Float.SIZE / 8)
                                .order(ByteOrder.nativeOrder()).asFloatBuffer();
                        positions.put(loader.positions).position(0);

                        FloatBuffer normals = ByteBuffer.allocateDirect(loader.normals.length * Float.SIZE / 8)
                                .order(ByteOrder.nativeOrder()).asFloatBuffer();
                        normals.put(loader.normals).position(0);

                        FloatBuffer textureCoordinates = ByteBuffer.allocateDirect(loader.textureCoordinates.length * Float.SIZE / 8)
                                .order(ByteOrder.nativeOrder()).asFloatBuffer();
                        textureCoordinates.put(loader.textureCoordinates).position(0);


                        ByteBuffer bb = ByteBuffer.allocateDirect(loader.positions.length * Float.SIZE / 8);
                        bb.order(ByteOrder.nativeOrder());
                        vertexBuffer = bb.asFloatBuffer();
                        vertexBuffer.put(positions);
                        vertexBuffer.position(0);

                        bb = ByteBuffer.allocateDirect(loader.textureCoordinates.length * Float.SIZE / 8);
                        bb.order(ByteOrder.nativeOrder());
                        textureCoordBuff = bb.asFloatBuffer();
                        textureCoordBuff.put(textureCoordinates);
                        textureCoordBuff.position(0);
                    }
                    isReadyObj = aBoolean;
                }
            });
        }else {
            isReadyObj = true;
        }

    }




        @Override
        public void draw() {

            GLES20.glUseProgram(shaderProgramId);

            GLES20.glVertexAttribPointer(positionHandle, 3, GLES20.GL_FLOAT, false,
                    0, vertexBuffer);
            GLES20.glEnableVertexAttribArray(positionHandle);

            GLES20.glVertexAttribPointer(textureCoordHandle, 2, GLES20.GL_FLOAT, false,
                    0, textureCoordBuff);
            GLES20.glEnableVertexAttribArray(textureCoordHandle);

            Matrix.setIdentityM(modelMatrix, 0);
            Matrix.multiplyMM(modelMatrix, 0, translation, 0, rotation, 0);
            Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scale, 0);
            Matrix.multiplyMM(modelMatrix, 0, transform, 0, modelMatrix, 0);


            Matrix.multiplyMM(localMvpMatrix, 0, projectionMatrix, 0, modelMatrix, 0);
            GLES20.glUniformMatrix4fv(mvpMatrixHandle, 1, false, localMvpMatrix, 0);

            GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
            GLES20.glUniform1i(textureHandle, 0);
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textureNames[0]);

            GLES20.glEnable(GLES20.GL_DEPTH_TEST);

            GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0, loader.positions.length / 3);


            GLES20.glDisableVertexAttribArray(positionHandle);
            GLES20.glDisableVertexAttribArray(textureCoordHandle);
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0);

            GLES20.glDisable(GLES20.GL_DEPTH_TEST);


//            GLES20.glUseProgram(shaderProgramId);
//            GLES20.glVertexAttribPointer(positionHandle, 3, GLES20.GL_FLOAT, false, 0, vertexBuffer);
//            GLES20.glEnableVertexAttribArray(positionHandle);
//            GLES20.glVertexAttribPointer(textureCoordHandle, 3, GLES20.GL_FLOAT, false, 0, textureCoordBuff);
//            GLES20.glEnableVertexAttribArray(textureCoordHandle);
//            GLES20.glVertexAttribPointer(colorHandle, 2, GLES20.GL_FLOAT, false, 0, colorBuffer);
//            GLES20.glEnableVertexAttribArray(colorHandle);
//            Matrix.setIdentityM(modelMatrix, 0);
//            Matrix.multiplyMM(modelMatrix, 0, translation, 0, rotation, 0);
//            Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scale, 0);
//            Matrix.multiplyMM(modelMatrix, 0, transform, 0, modelMatrix, 0);
//            Matrix.multiplyMM(localMvpMatrix, 0, projectionMatrix, 0, modelMatrix, 0);
//            GLES20.glUniformMatrix4fv(mvpMatrixHandle, 1, false, localMvpMatrix, 0);
//            GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
//            GLES20.glUniform1i(textureHandle, 0);
//            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textureNames[0]);
//            GLES20.glEnable(GLES20.GL_DEPTH_TEST);
//            GLES20.glDrawElements(GLES20.GL_TRIANGLES, INDEX_BUF.length,
//                    GLES20.GL_UNSIGNED_SHORT, indexBuffer);
//            GLES20.glDisableVertexAttribArray(positionHandle);
//            GLES20.glDisableVertexAttribArray(textureCoordHandle);
//            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0);
//            GLES20.glDisable(GLES20.GL_DEPTH_TEST);
        }

        public void setTextureBitmap(Bitmap texture) {
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textureNames[0]);
            GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, texture, 0);
            GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);
            GLES20.glEnable(GLES20.GL_BLEND);
            texture.recycle();
        }
    }












