package com.sisnet.arqr;

import com.maxst.ar.TrackedImage;

abstract class BackgroundRenderer extends BaseRenderer {
    abstract void draw(TrackedImage trackedImage);
}
