package com.sisnet.arqr;

import android.app.Activity;
import android.graphics.Bitmap;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.util.Log;
import android.widget.MediaController;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.maxst.ar.CameraDevice;
import com.maxst.ar.MaxstAR;
import com.maxst.ar.MaxstARUtil;
import com.maxst.ar.Trackable;
import com.maxst.ar.TrackedImage;
import com.maxst.ar.TrackerManager;
import com.maxst.ar.TrackingResult;
import com.maxst.ar.TrackingState;
import com.maxst.videoplayer.VideoPlayer;
import com.sisnet.arqr.Obj.ObjRenderer;
import com.sisnet.arqr.objloader.ObjLoader;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public class QrCodeTrackerRenderer implements GLSurfaceView.Renderer {
    public static final String TAG = QrCodeTrackerRenderer.class.getSimpleName();
    public float angle = 0;
    int typeQr=-2;
    float w = 1 , h = 1 ;
    public SimpleExoPlayer exoPlayer = null;
    private TexturedCubeRenderer texturedLoad,texturedPlay,texturedError,texturedInfo = new TexturedCubeRenderer();
    ObjRenderer objRenderer;

    public VideoRenderer videoRenderer;
    public VideoPlayer player;
    private int surfaceWidth;
    private int surfaceHeight;
    private Yuv420spRenderer backgroundCameraQuad;
    Map<Integer, VideoPlayer> savePlayer = new HashMap<>();
    List<TexturedCubeRenderer> infos = new ArrayList<>();
    private final AppCompatActivity activity;
    private BackgroundRenderHelper backgroundRenderHelper;
    public boolean isUrl = true;
    public boolean isObj = false;

    QrCodeTrackerRenderer(AppCompatActivity activity) {
        this.activity = activity;

    }

    public void loadInfo(int type){
        this.typeQr = type;
        texturedInfo = infos.get(type);
        isUrl = true;
        isObj = false;
    }

    public void loadInfo(String obj,String texture){
        isObj = true;
        isUrl = false;
        objRenderer.loadData(obj);
    }

    public void setInfo(String url,int width,int height,int id,float ratio){
        typeQr = -1;
        isUrl = false;
        isObj = false;

        player = new VideoPlayer(activity);
        if (width < height){
           h =  (float)height / (float)width * ratio;
           w = ratio;
        } else {
            h =  ratio;
            w =  (float)width / (float)height * ratio;
        }
        player.hs = h;
        player.ws = w;
        videoRenderer.setVideoPlayer(player);
        videoRenderer.videoSizeAcquired = false;
        Log.i("abzxzxmn", "setInfo: "+url);
        player.openVideo(url,width,height);
        exoPlayer = player.exoPlayer;

//        if (player.exoPlayer.isPlaying()){
//            player.setState(VideoPlayer.STATE_PLAYING);
//        }else if (player.exoPlayer.getPlaybackState() ==ExoPlayer.STATE_READY){
//            player.setState(VideoPlayer.STATE_READY);
//            if (!player.exoPlayer.getPlayWhenReady()){
//                player.setState(VideoPlayer.STATE_PAUSE);
//            }
//        }else if (player.exoPlayer.getPlaybackState() ==ExoPlayer.STATE_ENDED){
//            player.setState(VideoPlayer.STATE_DESTROYED);
//        }else if (player.exoPlayer.getPlaybackState() ==ExoPlayer.STATE_IDLE){
//            player.setState(VideoPlayer.STATE_ERROR);
//        }else {
//            player.setState(VideoPlayer.STATE_NONE);
//        }
        savePlayer.put(id,player);
    }

    public void loadInfo(int id,int width,int height,String url,float ratio) {
        typeQr = -1;
        isUrl = false;
        isObj = false;

        VideoPlayer simpleExoPlayer = savePlayer.get(id);
        exoPlayer = simpleExoPlayer.exoPlayer;
        videoRenderer.setVideoPlayer(simpleExoPlayer);
        videoRenderer.videoSizeAcquired = false;
        if (simpleExoPlayer != null && simpleExoPlayer.exoPlayer != null){
            h = simpleExoPlayer.hs;
            w = simpleExoPlayer.ws;
            if (simpleExoPlayer.exoPlayer.isPlaying()){
                player.setState(VideoPlayer.STATE_PLAYING);
            }else if (simpleExoPlayer.exoPlayer.getPlaybackState() ==ExoPlayer.STATE_READY){
                player.setState(VideoPlayer.STATE_READY);
                if (!simpleExoPlayer.exoPlayer.getPlayWhenReady()){
                    player.setState(VideoPlayer.STATE_PAUSE);
                }
            }else if (simpleExoPlayer.exoPlayer.getPlaybackState() ==ExoPlayer.STATE_ENDED){
                player.setState(VideoPlayer.STATE_DESTROYED);
            }else if (simpleExoPlayer.exoPlayer.getPlaybackState() ==ExoPlayer.STATE_IDLE){
                player.setState(VideoPlayer.STATE_ERROR);
            }else {
                player.setState(VideoPlayer.STATE_NONE);
            }
        }else {
            setInfo(url,width,height,id,ratio);
        }
    }


    @Override
    public void onSurfaceCreated(GL10 unused, EGLConfig config) {
        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);



        backgroundCameraQuad = new Yuv420spRenderer();
        videoRenderer = new VideoRenderer();
        objRenderer = new ObjRenderer(ViewModelProviders.of(activity).get(ObjLoader.class),activity);

        texturedLoad= new TexturedCubeRenderer();
        texturedPlay= new TexturedCubeRenderer();
        texturedError = new TexturedCubeRenderer();

        for (int i = 0 ; i<10 ; i++){
            TexturedCubeRenderer textured = new TexturedCubeRenderer();
            textured.type = i;
            textured.setTextureBitmap((MaxstARUtil.getBitmapFromAsset(i+".png", activity.getAssets())));
            infos.add(textured);
        }

        objRenderer.setTextureBitmap(MaxstARUtil.getBitmapFromAsset("a.jpg", activity.getAssets()));

        texturedLoad.setTextureBitmap(MaxstARUtil.getBitmapFromAsset("load.png", activity.getAssets()));
        texturedError.setTextureBitmap(MaxstARUtil.getBitmapFromAsset("error.png", activity.getAssets()));
        texturedPlay.setTextureBitmap(MaxstARUtil.getBitmapFromAsset("play.png", activity.getAssets()));
        backgroundRenderHelper = new BackgroundRenderHelper();
    }

    @Override
    public void onSurfaceChanged(GL10 unused, int width, int height) {
        surfaceWidth = width;
        surfaceHeight = height;
        MaxstAR.onSurfaceChanged(width, height);
    }


    @Override
    public void onDrawFrame(GL10 unused) {
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);
        GLES20.glViewport(0, 0, surfaceWidth, surfaceHeight);
        TrackingState state = TrackerManager.getInstance().updateTrackingState();
        TrackingResult trackingResult = state.getTrackingResult();
        TrackedImage image = state.getImage();
        float[] backgroundPlaneProjectionMatrix = CameraDevice.getInstance().getBackgroundPlaneProjectionMatrix();
        backgroundRenderHelper.drawBackground(image, backgroundPlaneProjectionMatrix);
        float[] projectionMatrix = CameraDevice.getInstance().getProjectionMatrix();
        GLES20.glEnable(GLES20.GL_DEPTH_TEST);
        if (trackingResult.getCount() == 0){
            typeQr = -2;
            if (videoRenderer.getVideoPlayer() != null) {
                videoRenderer.getVideoPlayer().pause();
            }
        }
        for (int i = 0; i < trackingResult.getCount(); i++) {
            Trackable trackable = trackingResult.getTrackable(i);

            videoRenderer.setProjectionMatrix(projectionMatrix);
            videoRenderer.setTransform(trackable.getPoseMatrix());
            videoRenderer.setTranslate(0.0f, 0.0f, -0.1f);
            videoRenderer.setScale(w, h, 1.0f);
            Log.i("VIDEOSIZE?", w + "  :  " + h);
            videoRenderer.draw();




            if (!isUrl&&videoRenderer.getVideoPlayer()!=null && videoRenderer.getVideoPlayer().exoPlayer !=null && !videoRenderer.getVideoPlayer().exoPlayer.isPlaying() &&typeQr==-1) {
                if (videoRenderer.getVideoPlayer().getState() == VideoPlayer.STATE_PAUSE ||videoRenderer.getVideoPlayer().getState() == VideoPlayer.STATE_READY) {
                    texturedPlay.setProjectionMatrix(projectionMatrix);
                    texturedPlay.setTransform(trackable.getPoseMatrix());
                    texturedPlay.setTranslate(0, 0, -0.2f);
                    texturedPlay.setScale(0.75f, 0.75f, 1.0f);
                    texturedPlay.draw();

                } else if (videoRenderer.getVideoPlayer().isLoading()) {
                    texturedLoad.setProjectionMatrix(projectionMatrix);
                    texturedLoad.setTransform(trackable.getPoseMatrix());
                    texturedLoad.setTranslate(0, 0, -0.2f);
                    texturedLoad.setScale(0.75f, 0.75f, 1.0f);
                    texturedLoad.setRotation(angle -= 1.5f, 0, 0, 1);
                    texturedLoad.draw();
                } else if (videoRenderer.getVideoPlayer().getState() == VideoPlayer.STATE_ERROR) {
                    texturedError.setProjectionMatrix(projectionMatrix);
                    texturedError.setTransform(trackable.getPoseMatrix());
                    texturedError.setTranslate(0, 0, -0.2f);
                    texturedError.setScale(0.75f, 0.75f, 1.0f);
                    texturedError.draw();
                }
            }else if (isUrl&&texturedInfo.type==typeQr){
                texturedInfo.setProjectionMatrix(projectionMatrix);
                texturedInfo.setTransform(trackable.getPoseMatrix());
                texturedInfo.setTranslate(0, 0, -0.2f);
                texturedInfo.setScale(0.75f, 0.75f, 1.0f);
                texturedInfo.draw();
            }else if (isObj){
                if (objRenderer.isReadyObj){
                    objRenderer.setProjectionMatrix(projectionMatrix);
                    objRenderer.setTransform(trackable.getPoseMatrix());
                    objRenderer.setTranslate(0.0f, 0.0f, -0.2f);
                    objRenderer.setScale(0.1f,0.1f, 0.1f);
                    objRenderer.setRotation(270,1,0,0);
                    objRenderer.draw();
                }else {
                    texturedLoad.setProjectionMatrix(projectionMatrix);
                    texturedLoad.setTransform(trackable.getPoseMatrix());
                    texturedLoad.setTranslate(0, 0, -0.2f);
                    texturedLoad.setScale(0.75f, 0.75f, 1.0f);
                    texturedLoad.setRotation(angle -= 1.5f, 0, 0, 1);
                    texturedLoad.draw();
                }
            }

        }
//        if (videoRenderer.getVideoPlayer().getState() == VideoPlayer.STATE_PLAYING) {
//            videoRenderer.getVideoPlayer().pause();
//        }
    }


    void destroyVideoPlayer() {
        if (videoRenderer != null){
            if (videoRenderer.getVideoPlayer() != null){
                videoRenderer.getVideoPlayer().destroy();
            }
        }
    }


}

