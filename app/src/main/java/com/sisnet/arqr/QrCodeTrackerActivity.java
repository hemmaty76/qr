package com.sisnet.arqr;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.ui.spherical.SingleTapListener;
import com.maxst.ar.CameraDevice;
import com.maxst.ar.MaxstAR;
import com.maxst.ar.MaxstARUtil;
import com.maxst.ar.ResultCode;
import com.maxst.ar.TrackerManager;
import com.maxst.ar.TrackingResult;
import com.maxst.videoplayer.VideoPlayer;
import com.sisnet.arqr.Obj.ObjRenderer;
import com.sisnet.arqr.ViewModel.InfoViewModel;
import com.sisnet.arqr.model.Info;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class QrCodeTrackerActivity extends ARActivity implements View.OnClickListener {

    private QrCodeTrackerRenderer QrCodeTargetRenderer;
    private GLSurfaceView glSurfaceView;
    private int preferCameraResolution = 0;
    private InfoViewModel viewModel;
     String currentResult = "";
    private boolean currentResultEmpty = true;
    private Parser parser;
    Map<Integer,Info> savesInfo = new HashMap<>();
    private boolean isFlash = false,isFull = false;
    ImageView flash,full;
    TextView title,desc;
    PlayerView playerView;
    View surface;
    int curentCamera = 0;
    View bgContent;
    ImageView location,phone,website;
    Info curentInfo;
    RecognizedQrCodeHandler handler;



    @Override
    protected void onResume() {
        super.onResume();

        glSurfaceView.onResume();
        TrackerManager.getInstance().startTracker(TrackerManager.TRACKER_TYPE_QR_TRACKER);

        ResultCode resultCode = ResultCode.Success;
        CameraDevice camera = CameraDevice.getInstance();
        switch (preferCameraResolution) {
            case 0:
                resultCode = camera.start(curentCamera, 640, 480);
                break;

            case 1:
                resultCode = camera.start(curentCamera, 1280, 720);
                break;

            case 2:
                resultCode = camera.start(curentCamera, 1920, 1080);
                break;
        }

        if (resultCode != ResultCode.Success) {
            Toast.makeText(this, "70qrcode error", Toast.LENGTH_SHORT).show();
            finish();
        }
        CameraDevice.getInstance().setFocusMode(CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUS_AUTO);

        MaxstAR.onResume();
        handler.sendEmptyMessage(0);


    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qr_code_tracker);
        playerView = findViewById(R.id.playerView);
        playerView.setVisibility(View.GONE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        viewModel = new ViewModelProvider(this).get(InfoViewModel.class);
        surface = findViewById(R.id.surface);
        flash = findViewById(R.id.flash);
        full = findViewById(R.id.camera);
        full.setVisibility(View.INVISIBLE);
        full.setEnabled(false);
        title = findViewById(R.id.title);
        desc = findViewById(R.id.description);
        location = findViewById(R.id.location);
        phone = findViewById(R.id.phone);
        website = findViewById(R.id.website);
        bgContent = findViewById(R.id.contacbg);
        bgContent.setVisibility(View.GONE);


        flash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isFlash){
                    isFlash = false;
                    CameraDevice.getInstance().setFlashLightMode(false);
                    flash.setImageResource(R.drawable.flashon);
                }else {
                    isFlash = true;
                    CameraDevice.getInstance().setFlashLightMode(true);
                    flash.setImageResource(R.drawable.flashoff);
                }
            }
        });



        QrCodeTargetRenderer = new QrCodeTrackerRenderer(this);
        glSurfaceView = (GLSurfaceView) findViewById(R.id.gl_surface_view);
        glSurfaceView.setEGLContextClientVersion(2);
        glSurfaceView.setRenderer(QrCodeTargetRenderer);

        handler = new RecognizedQrCodeHandler(this);
        TrackerManager.getInstance().addTrackerData("visit.2dmap", true);




        glSurfaceView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!QrCodeTargetRenderer.isUrl){

                    try {
                        if (QrCodeTargetRenderer.videoRenderer.getVideoPlayer().exoPlayer.isPlaying()){
                            QrCodeTargetRenderer.videoRenderer.getVideoPlayer().pause();
                        }else if (!QrCodeTargetRenderer.videoRenderer.getVideoPlayer().exoPlayer.isPlaying()&&(QrCodeTargetRenderer.videoRenderer.getVideoPlayer().getState() == VideoPlayer.STATE_PAUSE ||QrCodeTargetRenderer.videoRenderer.getVideoPlayer().getState() == VideoPlayer.STATE_READY)){
                            QrCodeTargetRenderer.videoRenderer.getVideoPlayer().start();
                        }
                    }catch (Exception e){

                    }

                }else {
                    cheakUri();
                }
            }
        });
        full.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isFull){
                    isFull = true;
                    currentResult="pause";
                    glSurfaceView.onPause();
                    surface.setVisibility(View.INVISIBLE);
                    glSurfaceView.setVisibility(View.INVISIBLE);
                    TrackerManager.getInstance().stopTracker();
                    MaxstAR.onPause();
                    playerView.setVisibility(View.VISIBLE);

                    playerView.setPlayer(QrCodeTargetRenderer.exoPlayer);
                    handler.removeCallbacksAndMessages(null);
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR);
                }
            }
        });
      }

    @Override
    public void onBackPressed() {
        if (isFull){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            isFull = false;
            glSurfaceView.onResume();
            TrackerManager.getInstance().startTracker(TrackerManager.TRACKER_TYPE_QR_TRACKER);
            MaxstAR.onResume();
            surface.setVisibility(View.VISIBLE);
            glSurfaceView.setVisibility(View.VISIBLE);
            handler.sendEmptyMessage(0);
            playerView.setPlayer(null);
            QrCodeTargetRenderer.exoPlayer.setPlayWhenReady(false);
            playerView.setVisibility(View.GONE);

        }else {
            super.onBackPressed();
        }
    }

    private void cheakUri() {
        if (parser != null){
            if (title != null && !title.getText().toString().equals("در حال بررسی...")){
                switch(parser.getType()){
                    case Parser.EVENT_TYPE:
                        Toast.makeText(this, "هنوز تعریف نشده است", Toast.LENGTH_SHORT).show();
                        break;
                    case Parser.VCARD_TYPE:
                        Toast.makeText(this, "هنوز تعریف نشده است", Toast.LENGTH_SHORT).show();
                        break;
                    case Parser.MECARD_TYPE:
                        Toast.makeText(this, "هنوز تعریف نشده است", Toast.LENGTH_SHORT).show();
                        break;
                    case Parser.TEXT_TYPE:
                        Toast.makeText(this, "هنوز تعریف نشده است", Toast.LENGTH_SHORT).show();
                        break;
                    case Parser.LOCATION_TYPE:
                        openMap(parser.getGeoCard().getLat(),parser.getGeoCard().getLon());
                        break;
                    case Parser.TELL_TYPE:
                        callPhone(parser.getPhoneTell());
                        break;
                    case Parser.SMS_TYPE:
                        sendSms(parser.getPhoneSms(),parser.getBodySms());
                        break;
                    case Parser.WIFI_TYPE:
                        openWifi(parser.getWifiCard().getSid(),parser.getWifiCard().getPassword());
                        break;
                    case Parser.URL_TYPE:
                        openUrl(parser.getUrl());
                        break;
                }
            }
        }
    }

    private void openMap(double latitude, double longitude) {
        String uriBegin = "geo:" + latitude + "," + longitude;
        String query = latitude + "," + longitude;
        String encodedQuery = Uri.encode(query);
        String uriString = uriBegin + "?q=" + encodedQuery + "&z=19";
        Uri uri = Uri.parse(uriString);
        Intent intent = new Intent(android.content.Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }
    private void openMap(String locat) {
        String add = locat.trim().replace(" ","");
        String uriBegin = "geo:" + add;
        String query = add;
        String encodedQuery = Uri.encode(query);
        String uriString = uriBegin + "?q=" + encodedQuery + "&z=19";
        Uri uri = Uri.parse(uriString);
        Intent intent = new Intent(android.content.Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }

    private void callPhone(String phoneTell) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + phoneTell));
        startActivity(intent);
    }

    private void sendSms(String phoneSms, String bodySms) {
        Intent smsIntent = new Intent(Intent.ACTION_VIEW);
        smsIntent.setType("vnd.android-dir/mms-sms");
        smsIntent.putExtra("address", phoneSms);
        smsIntent.putExtra("sms_body",bodySms);
        startActivity(smsIntent);
    }

    public void setTitle(String title){
        this.title.setText(title);
    }

    public void setDescription(String title){
        this.desc.setText(title);
    }

    public void openWifi(String ssid,String key){
        WifiConfiguration wifiConfig = new WifiConfiguration();
        wifiConfig.SSID = String.format("\"%s\"", ssid);
        wifiConfig.preSharedKey = String.format("\"%s\"", key);
        WifiManager wifiManager = (WifiManager)getApplicationContext().getSystemService(WIFI_SERVICE);
        int netId = wifiManager.addNetwork(wifiConfig);
        wifiManager.disconnect();
        wifiManager.enableNetwork(netId, true);
        wifiManager.reconnect();

    }

    public void openUrl(String url){
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }

    private void cheakData(int id){
        Log.i("abzxzxmnq", "****: " +id + "");
        QrCodeTargetRenderer.isUrl = true;

        if (savesInfo.containsKey(id)){
            Info info = savesInfo.get(id);
            creatContentClick(info);
            bgContent.setVisibility(View.VISIBLE);
            QrCodeTargetRenderer.loadInfo(id,info.getVideoWeigh(),info.getVideoHeight(),info.getVideoLink(),info.getRatio());
            setTitle(savesInfo.get(id).getTitle());
            setDescription(savesInfo.get(id).getDescription());
            full.setVisibility(View.VISIBLE);
            full.setEnabled(true);
//            QrCodeTargetRenderer.setInfo(savesInfo.get(id).getVideoLink(),savesInfo.get(id).getVideoWeigh(),savesInfo.get(id).getVideoHeight(),id);
        }else {
            viewModel.getInfo(id).observe(QrCodeTrackerActivity.this, result ->{
                switch (result.getStatus()){
                    case LOADING:
                        setTitle("درحال اتصال");
                        setDescription("درحال اتصال");
                        break;
                    case SUCCESS:
                        Info info = result.getData();
                        creatContentClick(info);
                        bgContent.setVisibility(View.VISIBLE);
                        setTitle(info.getTitle());
                        setDescription(info.getDescription());
                        savesInfo.put(id, info);
                        full.setVisibility(View.VISIBLE);
                        full.setEnabled(true);
                        QrCodeTargetRenderer.setInfo(info.getVideoLink(), info.getVideoWeigh(), info.getVideoHeight(), info.getId(),info.getRatio());

                        break;
                    case ERROR:
                        setTitle("خطا در بارگذاری محتوای هوشمند");
                        setDescription("این یک محتوای هوشمند است و برای نمایش آن نیاز به اینترنت است");
                        break;

                }
            });
       }
    }


    private void cheakData(String url) {
        Parser parser = new Parser(url);
        creatContentClick(null);
        this.parser = parser;
        QrCodeTargetRenderer.loadInfo(parser.getType());
        full.setVisibility(View.INVISIBLE);
        full.setEnabled(false);
//////////////////////***************************************************/////////////////////////////////
        if (parser != null){
            switch(parser.getType()){
                case Parser.EVENT_TYPE:
                    setTitle("رویداد");
                    setDescription(parser.getvEvent().getSummary());
                    break;
                case Parser.VCARD_TYPE:
                    setTitle("کارت ویزیت");
                    setDescription(parser.getvCard().getTitle());
                    break;
                case Parser.MECARD_TYPE:
                    setTitle("مشخصات فردی");
                    setDescription(parser.getMeCard().getDate());
                    break;
                case Parser.LOCATION_TYPE:
                    setTitle("مکان");
                    setDescription(parser.getGeoCard().buildString());
                    break;
                case Parser.TELL_TYPE:
                    setTitle("شماره تلفن");
                    setDescription(parser.getPhoneTell());
                    break;
                case Parser.SMS_TYPE:
                    setTitle("ارسال پیامک");
                    setDescription(parser.getBodySms());
                    break;
                case Parser.WIFI_TYPE:
                    setTitle("وای فای");
                    setDescription(parser.getWifiCard().getSid());
                    break;
                case Parser.TEXT_TYPE:
                    setTitle("متن");
                    setDescription(parser.getText());
                    break;
                case Parser.URL_TYPE:
                    setTitle("آدرس اینترنتی");
                    break;
            }
        }

    }

    private void cheakData(String url,boolean b){
        setTitle("ماشین سه بعدی");
        setDescription("تصویر نمونه سه بعدی ماشین با فرمت .obj به همراه تکستچر");
        QrCodeTargetRenderer.loadInfo(url,url);
    }

    public void creatContentClick(Info info){
        curentInfo = info;
        if (info == null)bgContent.setVisibility(View.GONE);
        else if (!info.getLocation().isEmpty()||!info.getPhone().isEmpty()||!info.getWebsite().isEmpty()){
            bgContent.setVisibility(View.VISIBLE);
            if (info.getLocation().isEmpty())location.setVisibility(View.GONE);else location.setVisibility(View.VISIBLE);
            if (info.getWebsite().isEmpty())website.setVisibility(View.GONE);else website.setVisibility(View.VISIBLE);
            if (info.getPhone().isEmpty())phone.setVisibility(View.GONE);else phone.setVisibility(View.VISIBLE);
        }else {
            bgContent.setVisibility(View.GONE);
        }
    }


    public void clickContent(View view){
        switch (view.getId()){
            case R.id.phone:
                callPhone(curentInfo.getPhone());
                break;
            case R.id.location:
                openMap(curentInfo.getLocation());
                break;
            case R.id.website:
                openUrl(curentInfo.getWebsite());
                break;
        }
    }



    @Override
    protected void onPause() {
        super.onPause();

        glSurfaceView.queueEvent(new Runnable() {
            @Override
            public void run() {
                QrCodeTargetRenderer.destroyVideoPlayer();
            }
        });

        glSurfaceView.onPause();

        TrackerManager.getInstance().stopTracker();
        CameraDevice.getInstance().stop();
        MaxstAR.onPause();
        handler.removeCallbacksAndMessages(null);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClick(View view) {

    }


    private static class RecognizedQrCodeHandler extends Handler {

        private WeakReference<QrCodeTrackerActivity> targetViewWeakReference;

        private RecognizedQrCodeHandler(QrCodeTrackerActivity target) {
            targetViewWeakReference = new WeakReference<QrCodeTrackerActivity>(target);
        }

        @Override
        public void handleMessage(Message msg) {
            QrCodeTrackerActivity target = targetViewWeakReference.get();

            if (target != null) {
                TrackingResult trackingResult = TrackerManager.getInstance().updateTrackingState().getTrackingResult();

                String recognizedQrCodeID = "";

                if (trackingResult.getCount() >= 1) {
                        for (int i = 0; i < trackingResult.getCount(); i++) {
                            recognizedQrCodeID += trackingResult.getTrackable(i).getName().toString();
                        }

                    if (!recognizedQrCodeID.equals(target.currentResult)){
                        target.currentResult = recognizedQrCodeID;
                        target.currentResultEmpty = false;
                        String url = recognizedQrCodeID.trim().toLowerCase();
                        if (url.startsWith("https://sisapp.ir/info.php?id=") || url.startsWith("http://sisapp.ir/info.php?id=")) {
                            int id = Integer.parseInt(url.charAt(url.length() - 1) + "");
                            target.cheakData(id);
                        }else if (url.startsWith("objtestex")){
                            target.cheakData("a.obj",false);
                        }else{
                            target.cheakData(recognizedQrCodeID);
                        }
                    }
                }else if (!target.currentResultEmpty){
                    Log.i("infoResumexo", "empty: ");
                    target.currentResult = "";
                    target.currentResultEmpty = true;
                    target.setTitle("در حال بررسی...");
                    target.setDescription("");
                }

                sendEmptyMessageDelayed(0, 200);
            }
        }
    }

}
